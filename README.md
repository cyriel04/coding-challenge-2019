# Overview
To complete this challenge, you will need to create a simple React web app, and provide us the source files.

The objective of this challenge is to guage your skills and approach to building a simple web app, given a set of screens and an API feed. We will also look at the generated HTML, CSS and JS output.

# The Test
Using the provided screens as a reference, you'll need to build a set of React components to render the app. You'll also need to request a JSON feed which you'll use to render the page.

Although this is a basic exercise, we'll be looking for simple, performant, well-designed and tested code.

Please include a README with any setup instructions, and any tests or other documentation you created as part of your solution.

# Details
You will need to build the following pages;

- Listing Page
- 'Single View' Page

A supplied JSON file - **/feed/data.json** - is included in the Bitbucket repository. You will need to use this data to render the page.

Please create components for each part of the page e.g. header, footer, etc.
Assets are provided in the **/assets** folder and the sketch file can be found in **/designs/adrenalin.sketch**.

Fonts used;

- Georgia
- Arial

# FAQ
## What framework, language, build tool should I use?
You may use whatever you like as long as you the project is built using React.