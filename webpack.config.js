const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	entry: './src/index.js',
	output: {
		publicPath: '/',
		path: path.join(__dirname, '/dist'),
		filename: 'bundle.js',
	},
	devServer: {
		historyApiFallback: true,
	},
	resolve: {
		modules: ['node_modules'],
		mainFiles: ['index', 'script', 'styles'],
		extensions: ['.js', '.json', '.scss'],
		alias: {
			components: path.resolve(__dirname, 'src/components'),
		},
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loaders: [
					{
						loader: 'babel-loader',
					},
				],
			},
			{
				test: /\.scss$/,
				use: [
					'style-loader',
					'css-loader',
					{
						loader: 'sass-loader',
					},
				],
			},
			{
				test: /\.(jpe?g|gif|png|svg|ico)$/,
				loaders: [
					{
						loader: 'file-loader',
					},
				],
			},
		],
	},
	optimization: {
		minimize: true,
		splitChunks: {
			chunks: 'all',
			cacheGroups: {
				base: {
					test: /[\\/]node_modules[\\/]/,
					name: 'base',
					chunks: 'all',
					reuseExistingChunk: true,
				},
				components: {
					test: /[\\/](components)[\\/]/,
					name: 'components',
					chunks: 'all',
					reuseExistingChunk: true,
				},
			},
		},
	},
	plugins: [new HtmlWebpackPlugin({ template: './src/index.html' })],
};
