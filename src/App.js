import React from 'react';
import { Switch, BrowserRouter, Route } from 'react-router-dom';
import Navigation from './components/Navigation';
import Footer from './components/Footer';
import ListingPage from './pages/ListingPage';
import NotFound from './pages/NotFound';
import SingleView from './pages/SingleView';

const App = () => {
	return (
		<div className="root">
			<BrowserRouter>
				<Navigation />
				<div className="content">
					<Switch>
						<Route exact path="/" component={ListingPage} />
						<Route path="/:slug" component={SingleView} />
						<Route component={NotFound} />
					</Switch>
				</div>
				<Footer />
			</BrowserRouter>
		</div>
	);
};

export default App;
