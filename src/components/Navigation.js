import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../assets/ad.svg';

const Navigation = () => {
	return (
		<div className="navigation">
			<div className="logo">
				<Link to="/">
					<img src={logo} alt="adrenalin" />
				</Link>
			</div>
			<div className="menu">
				<ul>
					<li>
						<Link to="/culture">Culture</Link>
					</li>
					<li>
						<Link to="/work">Work</Link>
					</li>
					<li>
						<Link to="/clients">Clients</Link>
					</li>
					<li>
						<Link to="/services">Services</Link>
					</li>
					<li>
						<Link to="/careers">Careers</Link>
					</li>
					<li>
						<Link to="/contact">Contact</Link>
					</li>
				</ul>
			</div>
		</div>
	);
};

export default Navigation;
