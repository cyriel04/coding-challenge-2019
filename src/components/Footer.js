import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../assets/ad.svg';

const Footer = () => {
	return (
		<div className="footer">
			<div className="logo">
				<Link to="/">
					<img src={logo} alt="adrenalin" />
				</Link>
			</div>

			<div className="list">
				<ul>
					<li>
						<Link to="/privacy">Privacy</Link>
					</li>
					<li>
						<Link to="/sitemap">Sitemap</Link>
					</li>
					<li>
						<a href="https://www.facebook.com" alt>
							Facebook
						</a>
					</li>
					<li>
						<a href="https://www.linkedin.com" alt>
							LinkedIn
						</a>
					</li>
					<li>
						<a href="https://www.instagram.com" alt>
							Instagram
						</a>
					</li>
					<li>
						<a href="https://www.twitter.com" alt>
							Twitter
						</a>
					</li>
				</ul>
			</div>
		</div>
	);
};

export default Footer;
