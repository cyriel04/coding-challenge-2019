import React from 'react';
import { Link } from 'react-router-dom';

const Card = (props) => {
	return (
		<div className="card">
			<p>{props.item.tag}</p>
			<img src={`../../assets/${props.item.thumb}`} alt />
			<h4>{props.item.title_long}</h4>
			<Link to={{ pathname: `/${props.item.slug}`, state: props.item }}>
				<div className="link">
					<hr />
					<span>VIEW CASE STUDY</span>
				</div>
			</Link>
		</div>
	);
};

export default Card;
