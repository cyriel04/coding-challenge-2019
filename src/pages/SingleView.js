import React, { useEffect, useState } from 'react';
import feed from '../../feed/data.json';
const SingleView = (props) => {
	const [data, setData] = useState({});

	useEffect(() => {
		scrollTo(0, 0);
	}, []);

	useEffect(() => {
		if (!props.location.state) {
			const params = props.history.location.pathname.split('/')[1];
			const filter = feed.find((item) => item.slug === params);
			return setData(filter);
		}
		return setData(props.location.state);
	}, [props.location]);

	return (
		<div className="svcontainer">
			<div className="img-container">
				<p>{data.tag}</p>
				<img src={`../../assets/${data.image}`} alt="" />
			</div>
			<div className="details">
				{/* <h1>{data.title}</h1> */}
				<h1>{data.title?.length > 25 ? `${data.title.substring(0, 18)}...` : data.title}</h1>

				{data.questions &&
					data.questions.map((res, key) => {
						return (
							<div key={key}>
								<h3>{`Question ${key + 1}`}</h3>
								<p>{res.length > 110 ? `${res.substring(0, 110)}...` : res}</p>
							</div>
						);
					})}
			</div>
		</div>
	);
};

export default SingleView;
